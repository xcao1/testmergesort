#include<iostream>
#include"MyVector.h"
#include"MyMergeSort.h"

using namespace std;
enum class READ{
	NUM,
	SPACE,
	OTHER
};
int main(){
	myvector<int> mNumbers;
	READ status = READ::OTHER;
	int num = 0;
	int number = 0;
	while(num != EOF){
		num = cin.get();
		if(num >= '0' && num <= '9'){// is digit
			if(status == READ::OTHER)
				number = num - '0';
			else
				number = number*10+(num-'0');
			status = READ::NUM;
		}else{// not a digit
			if(status == READ::NUM)
					mNumbers.push_back(number);
			number = 0;
			if(num == ' ' || num == '\n'){
				status = READ::SPACE;
			}else{
				status = READ::OTHER;
			}
		}
	}
	cout<<"\nThe input numbers :"<<endl;
	for(int i = 0; i != mNumbers.size(); i++){
		cout<<mNumbers[i]<<" "<<flush;
	}
	endl(cout);

	int size = mNumbers.size();
	int *numbers = new int [size];
	mNumbers.GetArray(numbers,size);

	MyMergeSort<int>::MergeSortAnArray(numbers,size);
	for(int i = 0; i != size; i++){
		cout<<numbers[i]<<" "<<flush;
	}

	system("pause");
	return 0;
}