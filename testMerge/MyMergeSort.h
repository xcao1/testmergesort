#ifndef __MY_MERGE_SORT__
#define __MY_MERGE_SORT__
// ======================================================
template <class Data>
class MyMergeSort{
	static bool Merge(Data *in_array, int begin, int mid, int end);
	static bool MergeSort(Data *in_array, int begin, int end);
public:
	static bool MergeSortAnArray(Data *in_array, int size);
};
// [begin , mid) merge with [mid , end)
template <class Data> 
bool MyMergeSort<Data>::Merge(Data *in_array, int begin, int mid, int end){
	
	int i = begin;
	int j = mid; 

	int size = end - begin;
	if(size == 0)
		return true;
	Data *temp = new Data [size];
	if(!temp)
		return false;
	int n = 0;

	while( i != mid && j != end && n != size){
		if(in_array[i]<=in_array[j]){
			temp[n] = in_array[i];
			i++;
		}else{
			temp[n] = in_array[j];
			j++;
		}
		n++;
	}
	while(i != mid){
		temp[n++] = in_array[i++];
	}
	while(j != end){
		temp[n++] = in_array[j++];
	}
	n = 0;
	while(n != size){
		in_array[begin+n] = temp[n];
		n++;
	}
	return true;
}
// merge sort a given array
// [begin, end), end point to the address next to the last element in the array
template <class Data>
bool MyMergeSort<Data>::MergeSort(Data *in_array, int begin, int end){
	if(begin < end){
		if(end - begin == 1)
			return true;
		int mid = (begin+end)/2;
		if(!MergeSort(in_array,begin,mid))
			return false;
		if(!MergeSort(in_array,mid,end))
			return false;
		if(!Merge(in_array,begin,mid,end))
			return false;
		return true;
	}else
		return false;
}
template <class Data> 
bool MyMergeSort<Data>::MergeSortAnArray(Data *in_array, int size){
	if(MergeSort(in_array,0,size))
		return true;
	else
		return false;
}























// ======================================================
#endif // !__MY_MERGE_SORT__
